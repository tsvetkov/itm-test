<?php

error_reporting(E_ALL ^ E_NOTICE);

require_once('./conf/_init.php');

Core::loadController();

$out = Core::controller()->run();

if ($out !== false) {
    Core::template()->out($out);
    Core::template()->parse();
    Core::template()->flush();
}
