CREATE TABLE film
(
  id             BIGINT UNSIGNED PRIMARY KEY NOT NULL,
  title          VARCHAR(500)                NOT NULL,
  original_title VARCHAR(500)                NOT NULL,
  country        VARCHAR(100)                NOT NULL,
  filming_start  YEAR,
  filming_end    YEAR,
  director       VARCHAR(200)                NOT NULL,
  actors         TEXT                        NOT NULL,
  length         INT,
  premiere       DATE,
  short_descr    VARCHAR(1000)               NOT NULL,
  descr          TEXT                        NOT NULL
);
CREATE TABLE genre
(
  id    BIGINT UNSIGNED PRIMARY KEY NOT NULL,
  title VARCHAR(100)                NOT NULL
);
CREATE TABLE film_genre
(
  film_id  BIGINT UNSIGNED NOT NULL,
  genre_id BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (film_id, genre_id)
);
