<?php

class Genre extends Model {

    static function storageTable() {
        return 'genre';
    }

    protected static function generatePk() {
        // todo да я знаю что это печально, но автоинкремент зло
        return (int)((time() - strtotime('2013-01-01')) + (microtime() * 1000) + rand(0, 100));
    }

    public function validateTitle($value) {
        if (!$value) {
            return 'Обязательное поле';
        }
    }

    public function getFilmsCount() {
        $films = 0;
        $data = Film::finder()->find('id in (select film_id from film_genre where genre_id = :id)',
            [':id' => $this->id])->order('title')->all();
        if ($data) {
            $films = $data->total();
        }
        return $films;
    }

    protected function post_delete() {
        Core::DB()->query('delete from film_genre where genre_id = ' .$this->pk());
    }
}