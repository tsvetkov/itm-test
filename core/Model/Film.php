<?php

class Film extends Model {

    private $genres;

    static function storageTable() {
        return 'film';
    }

    protected static function generatePk() {
        // todo да я знаю что это печально, но автоинкремент зло
        return (int)((time() - strtotime('2013-01-01')) + (microtime() * 1000) + rand(0, 100));
    }

    public function setGenres($genres) {
        if (is_array($genres)) {
            $this->genres = $genres;
        } else {
            $this->genres = $genres ? array_map('trim', explode(',', $genres)) : [];
        }
    }

    public function getGenres() {
        $genres = [];
        $data = Genre::finder()->find('id in (select genre_id from film_genre where film_id = :id)',
            [':id' => $this->id])->order('title')->all();
        if ($data) {
            foreach ($data as $v) {
                $genres[] = $v->title;
            }
        }
        return $genres;
    }

    public function getGenresVerbose() {
        return implode(', ', $this->getGenres());
    }

    public function getLengthVerbose() {
        if ($this->length) {
            $sec = (int)($this->length % 60);
            $min = (int)(($this->length % (60 * 60)) / 60);
            $hour = (int)($this->length / (60 * 60));
            $time = sprintf('%d:%02d:%02d', $hour, $min, $sec);
            return $time;
        } else {
            return '';
        }

    }

    public function getPremiereVerbose() {
        return $this->premiere ? date('d.m.Y', strtotime($this->premiere)) : '';
    }

    function image() {
        return file_exists(Core::config('upload_dir') . '/' . $this->id . '.jpg');
    }

    public function getImageUrl() {
        return
            str_replace(Core::config('project_dir'), '', Core::config('upload_dir')) . '/' . $this->id . '.jpg';
    }

    public function getSmallImageUrl() {
        return
            str_replace(Core::config('project_dir'), '', Core::config('upload_dir')) . '/' . $this->id . '_100x145.jpg';
    }

    public function getMediumImageUrl() {
        return
            str_replace(Core::config('project_dir'), '', Core::config('upload_dir')) . '/' . $this->id . '_150x218.jpg';
    }

    public function validateTitle($value) {
        if (!$value) {
            return 'Обязательное поле';
        }
    }

    protected function post_delete() {
        if ($this->image()) {
            // todo таки не кошерно, но лень
            @unlink(Core::config('upload_dir') . '/' . $this->id . '.jpg');
            @unlink(Core::config('upload_dir') . '/' . $this->id . '_100x145.jpg');
            @unlink(Core::config('upload_dir') . '/' . $this->id . '_150x218.jpg');
        }
        Core::DB()->query('delete from film_genre where film_id = ' .$this->pk());
    }

    protected function post_save() {
        if (!is_array($this->genres)) return;
        $old_genres = $this->getGenres();
        $add_genres = array_diff($this->genres, $old_genres);
        $del_genres = array_diff($old_genres, $this->genres);
        // todo как-то это добро надо оптимизировать, что ли
        foreach ($del_genres as $v) {
            $g = Genre::finder()->find('title = :title', [':title'=>$v])->get();
            Core::DB()->query('delete from film_genre where film_id = ' .$this->pk(). ' and genre_id = ' . $g->pk());
        }
        foreach ($add_genres as $v) {
            $g = Genre::finder()->find('title = :title', [':title'=>$v])->get();
            if (!$g) {
                $g = Genre::bind(0);
                $g->title = $v;
                $g->save();
            }
            Core::DB()->query('insert into film_genre (film_id, genre_id) values (' . $this->pk() . ' , ' . $g->pk() . ')');
        }
    }

}