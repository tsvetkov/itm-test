<?php

abstract class Model {

    private $__pk = null;
    private $__data = [];
    private $__changes = [];

    /** @var Set[] */
    private static $__registry = [];

    final private function __construct($pk) {
        $this->__pk = $pk;
    }

    static function storageTable() {
        $class = get_called_class();

        return strtolower(preg_replace('/([^A-Z_])([A-Z])/', "$1_$2", str_replace('\\', '__', $class)));
    }

    static function pkField() {
        return 'id';
    }

    protected static function generatePk() {
        return null;
    }

    final function __get($key) {
        if ($key == static::pkField()) {
            return $this->__pk;
        }

        if (array_key_exists($key, $this->__changes)) {
            return $this->__changes[$key];
        }

        $this->load();

        return array_key_exists($key, $this->__data) ?
            $this->__data[$key] : null;
    }

    final function __set($key, $val) {
        if ($key == static::pkField()) {
            throw new Exception('Cannot overwrite PK');
        }

        $exists = $this->is_loaded()
            && array_key_exists($key, $this->__data)
            && $this->__data[$key] === $val;

        if (!$exists) {
            $this->__changes[$key] = $val;
        } else {
            unset($this->__changes[$key]);
        }
    }

    final function __isset($key) {
        return $this->__get($key) !== null;
    }

    final function __unset($key) {
        $this->__set($key, null);
    }

    final function __clone() {
        throw new Exception('Models cannot be cloned directly');
    }

    final function is_changed() {
        return is_array($this->__changes) && count($this->__changes);
    }

    final function exists() {
        $this->load();

        if ($this->is_loaded()) {
            return true;
        }

        return false;
    }

    final function save() {
        $this->pre_save();
        if ($this->is_changed()) {
            $query = 'insert into `' . static::storageTable() .
                '` (`' . static::pkField() . '`, `' . implode('`, `', array_keys($this->__changes)) . '`) ' .
                ' values ("' . $this->__pk . '", ' .
                implode(', ',
                    array_map(
                        function ($value) {
                            return $value === null ? 'NULL' : '"' . $value . '"';
                        },
                        array_values($this->__changes))) . ') ' .
                ' on duplicate key update ' .
                implode(', ',
                    array_map(function ($key) {
                            return '`' . $key . '`=VALUES(`' . $key . '`)';
                        },
                        array_keys($this->__changes))) .
                '';
            Core::DB()->query($query);
            $this->__data    = array_merge($this->__data, $this->__changes);
            $this->__changes = [];
        }
        $this->post_save();
        return $this;
    }

    final function delete() {
        if ($this->exists()) {
            $this->pre_delete();
            $query = 'delete from `' . static::storageTable() . '` where `' . static::pkField() . '` = "' . $this->pk() . '"';
            Core::DB()->query($query);
            $this->__data    = [];
            $this->__changes = [];
            $this->post_delete();
        }
        return $this;
    }

    final function load() {
        return $this->is_loaded() ? $this : $this->reload();
    }

    final function reload() {
        $data = self::finder()->findByPk($this->__pk)->data();
        if (is_array($data)) {
            unset($data[self::pkField()]);
            $this->__data = $data;
        }
        return $this;
    }

    final function is_loaded() {
        return is_array($this->__data) && count($this->__data);
    }

    final function revert() {
        $this->__changes = [];
        return $this;
    }

    final function pk() {
        return $this->__pk;
    }

    /**
     * @param $pk
     * @return self
     * @throws Exception
     */
    final static function bind($pk) {
        if (!is_scalar($pk)) {
            throw new Exception('Model PK must be scalar');
        }

        if (empty($pk)) {
            $pk = static::generatePk();
            if (empty($pk)) {
                throw new Exception('Cannot bind to empty PK');
            }
        }

        $class = '\\' . get_called_class();

        if ($class == '\\' . __CLASS__) {
            throw new Exception('Cannot bind Model to itself');
        }

        if (!isset(self::$__registry[$class])) {
            self::$__registry[$class] = new Set($class);
            self::$__registry[$class]->add($model = new $class($pk));
        } elseif (($model = self::$__registry[$class]->getByPk($pk)) === false) {
            self::$__registry[$class]->add($model = new $class($pk));
        }

        return $model;
    }

    final static function finder() {
        $class = '\\' . get_called_class();
        return new Finder($class);
    }

    final function __import($data) {
        if (is_array($data)) {
            unset($data[self::pkField()]);
            $this->__data = $data;
        }
    }

    final function validate() {
        $errors = [];
        foreach ($this->__changes as $key => $value) {
            $validator = 'validate' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
            if (method_exists($this, $validator)) {
                $error = $this->$validator($value);
                if ($error) {
                    $errors[$key] = $error;
                }
            }
        }
        return $errors;
    }

    final function export() {
        return $this->__data;
    }

    protected function pre_save() {
    }

    protected function post_save() {
    }

    protected function pre_delete() {
    }

    protected function post_delete() {
    }

}