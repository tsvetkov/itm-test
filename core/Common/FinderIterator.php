<?php

class FinderIterator implements Iterator, Countable {

    /** @var Model */
    private $modelClass;
    /** @var PDOStatement */
    private $statement;
    private $start;
    private $limit;
    private $total;

    private $current;
    private $index;

    function __construct($class, $statement, $total, $limit, $start = 1) {
        if (!is_subclass_of($class, '\\Model')) {
            throw new InvalidArgumentException('Invalid $class given');
        }
        $this->modelClass = $class;

        $this->total     = $total;
        $this->start     = $start;
        $this->limit     = $limit;
        $this->statement = $statement;
        $this->index     = $start ?: 0;
    }

    public function current() {
        if ($this->current) {
            $class = $this->modelClass;
            $model = $class::bind($this->current[$class::pkField()]);
            $model->__import($this->current);
            return $model;
        }
        return null;
    }

    public function next() {
        $this->current = $this->statement->fetch(PDO::FETCH_ASSOC);
        $this->index++;
    }

    public function key() {
        if ($this->current) {
            return $this->index;
        }
        return null;
    }

    public function valid() {
        if ($this->current) {
            return true;
        }
        return false;
    }

    public function rewind() {
        $this->current = $this->statement->fetch(PDO::FETCH_ASSOC);
        $this->index++;
    }

    public function total() {
        return $this->total;
    }

    public function count() {
        return $this->total;
    }
}
