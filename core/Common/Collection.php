<?php

abstract class Collection implements Countable {
    private $__class;

    function __construct($class, $models = null) {
        if (!is_subclass_of($class, '\\Model')) {
            throw new InvalidArgumentException('Invalid $class given');
        }

        $this->__class = ($class[0] != '\\') ? ('\\' . $class) : $class;

        if ($models !== null) {
            $this->import($models);
        }
    }

    final function getClass() {
        return $this->__class;
    }

    abstract function clear();

    abstract function import($models);

    abstract function export();
}
