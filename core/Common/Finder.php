<?php

class Finder {

    /** @var Model */
    private $modelClass;
    /** @var PDOStatement */
    private $statement;
    private $query = [];
    private $start;
    private $limit;
    private $order = [];
    private $params = [];

    function __construct($class) {
        if (!is_subclass_of($class, '\\Model')) {
            throw new InvalidArgumentException('Invalid $class given');
        }
        $this->modelClass = $class;
    }

    function reset() {
        $this->statement = null;
        $this->query = [];
        $this->start = null;
        $this->limit = null;
        $this->order = [];
        $this->params = [];
    }

    /**
     * @param $pk
     * @return Model
     */
    function getByPk($pk) {
        return $this->findByPk($pk)->get();
    }

    /**
     * @param $pk
     * @return $this
     */
    function findByPk($pk) {
        $class = $this->modelClass;
        $this->find($class::pkField() . ' = :pk', [':pk' => $pk]);
        return $this;
    }

    /**
     * @param $query
     * @param array $params
     * @return Finder
     */
    function find($query, $params = []) {
        $this->query[] = $query;
        $this->params  = array_merge($this->params, $params);
        return $this;
    }

    /**
     * @param $order
     * @return Finder
     */
    function order($order) {
        $this->order[] = $order;
        return $this;
    }

    /**
     * @return bool|FinderIterator
     */
    function all() {
        $class = $this->modelClass;
        if (!$this->statement) {
            $this->prepare();
        }
        $res = $this->statement->execute($this->params);
        if ($res) {
            $total = Core::DB()->query('SELECT FOUND_ROWS()')->fetchColumn();
            return new FinderIterator($this->modelClass, $this->statement, $total, $this->limit, $this->start);
        }
        return false;
    }

    /**
     * @return Model|bool
     */
    function get() {
        $class = $this->modelClass;
        if (!$this->statement) {
            $this->prepare();
        }
        $res = $this->statement->execute($this->params);
        $data  = $this->statement->fetch(PDO::FETCH_ASSOC);
        if ($data) {
            $model = $class::bind($data[$class::pkField()]);
            $model->__import($data);
            return $model;
        }
        return false;
    }


    /**
     * @return array|bool
     */
    function data() {
        $class = $this->modelClass;
        if (!$this->statement) {
            $this->prepare();
        }
        $res = $this->statement->execute($this->params);
        if ($res) {
            $total = Core::DB()->query('SELECT FOUND_ROWS()')->fetchColumn();
            if ($total == 1) {
                return $this->statement->fetch(PDO::FETCH_ASSOC);
            } else {
                return $this->statement->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        return false;
    }

    private function prepare() {
        $class = $this->modelClass;
        if (!$this->statement) {
            $query = 'select SQL_CALC_FOUND_ROWS * from `' . $class::storageTable() . '`';
            if ($this->query) {
                $query .= ' where ' . implode(' and ', $this->query);
            }
            if ($this->order) {
                $query .= ' order by ' . implode(', ', $this->order);
            }
            if ($this->limit) {
                if ($this->start) {
                    $query .= ' limit ' . $this->start . ',' . $this->limit;
                } else {
                    $query .= ' limit ' . $this->limit;
                }
            }
            $this->statement = Core::DB()->prepare($query);
        }
        return $this;
    }

    public function start($start) {
        $this->start = $start;
        return $this;
    }

    public function limit($limit) {
        $this->limit = $limit;
        return $this;
    }


}