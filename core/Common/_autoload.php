<?php

return [
    'Controller'     => 'Controller.php',
    'Template'       => 'Template.php',
    'Model'          => 'Model.php',
    'Finder'         => 'Finder.php',
    'FinderIterator' => 'FinderIterator.php',
    'Set'            => 'Set.php',
    'Collection'     => 'Collection.php',
];

