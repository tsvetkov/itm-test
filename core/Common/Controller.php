<?php

abstract class Controller {
    protected $request;
    protected $post;
    protected $get;
    protected $files;
    protected $method;
    protected $cookie;
    protected $view;
    protected $template;
    protected $out = '';

    final function __construct() {
        $this->post   = Core::requestPost();
        $this->get    = Core::requestGet();
        $this->files  = Core::requestFiles();
        $this->method = Core::requestMethod();
        $this->cookie = Core::requestCookie();
    }

    final function request($parts) {
        $this->request = $parts;
    }

    function init() {
    }

    function finalize() {
    }

    final function run() {
        $this->init();
        $action = 'default';
        if (!empty($this->request[0])) {
            $action = $this->request[0];
        }
        $method = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $action)))) . 'Action';
        if (method_exists($this, $method)) {
            array_shift($this->request);
        } else {
            $action = 'default';
            $method = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $action)))) . 'Action';
        }

        $this->view = $action;

        $this->out = $this->$method();
        $this->finalize();

        return $this->out;
    }

    function defaultAction() {
        return '';
    }

    protected final function view() {
        if (empty($this->template)) {
            $class    = get_class($this);
            $template = strtolower(substr($class, 0, -strlen(__CLASS__))) . '/' .
                $this->view;

            $this->template = new Template();
            $this->template->setTemplate($template);
        }
        return $this->template;
    }

}

