<?php

class Set extends Collection {
    protected $_models = [];

    protected $_count = 0;

    function add(Model $model) {
        if ('\\' . get_class($model) != $this->getClass()) {
            throw new Exception('Invalid model class');
        }

        $node =& $this->_node($model->pk());

        if ($node === null) {
            $node = $model;
            $this->_count++;
        }

        return $this;
    }

    function clear() {
        $this->_models = [];
        $this->_count  = 0;
        return $this;
    }


    function count() {
        return $this->_count;
    }

    function import($models) {
        if ($models instanceof Set) {
            $models = $models->export();
        }

        if (!is_array($models)) {
            throw new Exception('Invalid $models given');
        }

        foreach ($models as $model) {
            $this->add($model);
        }

        return $this;
    }


    function export() {
        $result = [];

        array_walk_recursive($this->_models,
            function ($model) use (&$result) {
                if ($model !== null) {
                    $result[] = $model;
                }
            });

        return $result;
    }

    function exclude(Model $model) {
        if ('\\' . get_class($model) != $this->getClass()) {
            throw new Exception('Invalid $models given');
        }

        $node =& $this->_node($model->pk());

        if ($node !== null) {
            $this->_count--;
            $node = null;
        }

        return $this;
    }


    function getByPk($pk) {
        $ref = & $this->_models[$pk];
        return $ref ? $ref : false;
    }


    protected function &_node($pk) {
        $ref = & $this->_models[$pk];
        return $ref;
    }
}
