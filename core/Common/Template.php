<?php

class Template {
    private $file;
    private $out;
    private $vars = [];

    function parse() {
        if ($this->file) {
            extract($this->vars);
            ob_start();
            include($this->file);
            $this->out = ob_get_contents();
            ob_end_clean();
        }
        return $this;
    }

    function setTemplate($tpl) {
        $this->file = Core::config('view_dir') . '/' . $tpl . '.php';
        return $this;
    }

    function assign($key, $value) {
        $this->vars[strtoupper($key)] = $value;
        return $this;
    }

    function append($key, $value) {
        $this->vars[strtoupper($key)] .= $value;
        return $this;
    }

    function out($value) {
        return $this->append('content', $value);
    }

    function flush() {
        echo $this->out;
        return $this;
    }

    function get() {
        return $this->out;
    }

}
