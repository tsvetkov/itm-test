<?php

function __autoload($className) {
    $sm = Core::autoload($className);
}

final class Core {
    const DEFAULT_TEMPLATE = 'main';

    private static $config = [];
    private static $db = [];
    private static $db_connect = [];
    private static $autoload = [];
    private static $base_url;
    private static $base_uri;
    private static $get;
    private static $cookie;
    private static $post;
    private static $files;
    private static $request_method;
    private static $uri;
    private static $uri_parts;
    private static $default_controller = 'MainController';
    private static $controller_uri;

    /** @var Controller $controller_class */
    private static $controller_class;
    /** @var Template $template */
    private static $template;

    static function init() {
        if (!empty(self::$config)) {
            throw new Exception('Do not init more!');
        }

        $config = $GLOBALS['config'];
        unset($GLOBALS['config']);

        self::$db = $config['db'];
        unset($config['db']);

        if (array_key_exists('default_controller', $config)) {
            self::$default_controller = $config['default_controller'];
            unset($config['default_controller']);
        }

        self::$config = $config;

        if (isset(self::$config['autoload']) && is_array(self::$config['autoload'])) {
            foreach (self::$config['autoload'] as $autoload) {
                self::addAutoload($autoload);
            }
        }

        unset(self::$config['autoload']);

        self::connect();

        self::parseRequest();

        self::$template = new Template();
        self::$template->setTemplate(self::DEFAULT_TEMPLATE);
        self::$template->assign('project', self::config('project'));
    }

    static function config($key = null) {
        if ($key) {
            return self::$config[$key];
        } else {
            return self::$config;
        }
    }

    /**
     * @param string $connect
     * @return PDO
     * @throws Exception
     */
    static function DB($connect = 'default') {
        if (!self::$db_connect[$connect]) {
            self::connect($connect);
        }
        if (self::$db_connect[$connect]) {
            return self::$db_connect[$connect];
        } else {
            throw new Exception('No connection for ' . $connect);
        }
    }

    static function autoload($className) {
        if (array_key_exists($className, self::$autoload)) {
            if (self::$autoload[$className] == false) {
                return false;
            }
            $file = self::$autoload[$className];
            if (file_exists($file)) {
                require_once($file);
            } else {
                throw new Exception('No require file: ' . $file);
            }
            return true;
        }
        return false;
    }

    static function addAutoload($path) {
        $path = self::$config['core_dir'] . '/' . $path;
        if (!is_dir($path)) {
            throw new Exception('No autoload path: ' . $path);
        }
        $file = $path . '/_autoload.php';
        if (!file_exists($file)) {
            throw new Exception('No autoload file: ' . $file);
        }

        $array = require($file);
        foreach ($array as $k => $v) {
            $array[$k] = $path . '/' . $v;
        }

        self::$autoload = array_merge(self::$autoload, $array);
    }

    static function template() {
        return self::$template;
    }

    static function uri($index = null) {
        if (is_int($index)) {
            return self::$uri_parts[$index + 0];
        } elseif ($index == 'all') {
            return self::$uri_parts;
        } else {
            return self::$uri;
        }
    }

    static function uriParts($cnt, $start = 0) {
        $i   = 0;
        $ret = '';
        while ($i < $cnt && $i + $start < count(self::$uri_parts)) {
            $ret .= '/' . self::$uri_parts[$i + $start];
            $i++;
        }
        return $ret;
    }

    static function controller() {
        return self::$controller_class;
    }

    static function requestPost($key = null) {
        if ($key) {
            return @self::$post[$key];
        } else {
            return self::$post;
        }
    }

    static function requestGet($key = null) {
        if ($key) {
            return @self::$get[$key];
        } else {
            return self::$get;
        }
    }

    static function requestCookie($key = null) {
        if ($key) {
            return @self::$cookie[$key];
        } else {
            return self::$cookie;
        }
    }

    static function requestFiles($key = null) {
        if ($key) {
            return @self::$files[$key];
        } else {
            return self::$files;
        }
    }

    static function requestMethod() {
        return self::$request_method;
    }

    static function redirect($path) {
        if (strlen($path) > 0 && substr($path, 0, 1) == '/') {
            $path = self::$base_uri . $path;
        }
        $path = str_replace('//', '/', $path);
        header("Location: {$path}\n\n");
        exit;
    }

    static private function connect($connect = 'default') {
        $db = false;
        if (isset(self::$db[$connect]) && !isset(self::$db_connect[$connect])) {
            $conn = self::$db[$connect];

            if (!isset($conn['charset'])) {
                $conn['charset'] = 'utf8';
            }

            try {
                $db = new PDO('mysql:dbname=' . $conn['name'] . ';host=' . $conn['host'], $conn['user'], $conn['password']);
            } catch (PDOException $e) {
                echo 'Подключение не удалось: ' . $e->getMessage();
            }
            if ($db) {
                $db->exec("SET NAMES '{$conn['charset']}'");
                $db->exec("SET CHARACTER SET '{$conn['charset']}'");
            }
            self::$db_connect[$connect] = $db;
        }
    }

    static private function parseRequest() {
        $base_uri = substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/'));

        $uri = substr(@$_SERVER['REQUEST_URI'], strrpos($_SERVER['PHP_SELF'], '/'));
        $uri = strpos($uri, '?') !== false ? substr($uri, 0, strpos($uri, '?')) : $uri;
        $p   = pathinfo($uri);

        if (array_key_exists('extension', $p) && $p['extension'] != '') {
            $uri = $p['dirname'] . '/' . basename($p['basename'], '.' . $p['extension']);
        }

        $base_url = ((array_key_exists('HTTPS', $_SERVER)
                && !empty($_SERVER['HTTPS'])) == "on"
                ? "https"
                : "http") . "://" . @$_SERVER['HTTP_HOST'] . $base_uri;

        $uri_parts = explode('/', $uri);
        if (is_array($uri_parts)) array_shift($uri_parts); else $uri_parts = [];

        self::$base_url       = $base_url;
        self::$base_uri       = $base_uri;
        self::$uri            = $uri;
        self::$uri_parts      = $uri_parts;
        self::$request_method = @$_SERVER['REQUEST_METHOD'];
        self::$post           = $_POST;
        self::$get            = $_GET;
        self::$files          = $_FILES;
        self::$cookie         = $_COOKIE;
    }

    static function run() {
        self::loadController();
        return self::controller()->run();
    }

    static function loadController() {
        $parts = self::$uri_parts;
        if (isset($parts[0])) {
            $controller_class = str_replace(' ', '', ucwords(str_replace('_', ' ', $parts[0]))) . 'Controller';
        } else {
            $controller_class = self::$default_controller;
        }

        $file = self::$config['controller_dir'] . '/' . $controller_class . '.php';
        if (is_readable($file)) {
            require_once($file);
            self::$controller_uri = $parts[0];
            array_shift($parts);
        } else {
            $controller_class     = self::$default_controller;
            self::$controller_uri = '';
            $file                 = self::$config['controller_dir'] . '/' . $controller_class . '.php';
            require_once($file);
        }

        self::$controller_class = new $controller_class();
        self::$controller_class->request($parts);
    }

}
