<?
/** @var Film $FILM */
if (!$ERROR) {
    ?>

    <h2><?= $FILM->title ?>
        <div class="pull-right">
            <a href="/edit/<?= $FILM->id ?>" class="btn btn-primary">Edit</a>
            <a href="/film/delete/<?= $FILM->id ?>" class="btn btn-danger">Delete</a>
        </div>
    </h2>
    <div class="row">
        <div class="col-md-2">
            <? if ($FILM->image()) { ?><img src='<?= $FILM->getMediumImageUrl() ?>' width="150" height="218" class="img-thumbnail"><? } ?>
        </div>
        <div class="col-md-10">
            <table class="table table-striped table-condensed">
                <tbody>

                <tr>
                    <th style="width: 25%">Название</th>
                    <td><strong><?= $FILM->title ?></strong></td>
                </tr>
                <? if ($FILM->original_title) { ?>
                    <tr>
                        <th>Оригинальное название</th>
                        <td><?= $FILM->original_title ?></td>
                    </tr>
                <? } ?>
                <tr>
                    <th>Анонс</th>
                    <td><?= $FILM->short_descr ?></td>
                </tr>
                <tr>
                    <th>Страна</th>
                    <td><?= $FILM->country ?></td>
                </tr>
                <tr>
                    <th>Продолжительность</th>
                    <td><?= $FILM->getLengthVerbose() ?></td>
                </tr>
                <tr>
                    <th>Дата премьеры</th>
                    <td><?= $FILM->getPremiereVerbose() ?></td>
                </tr>
                <tr>
                    <th>Годы съёмки</th>
                    <td><?= $FILM->filming_start ?> - <?= $FILM->filming_end ?></td>
                </tr>
                <tr>
                    <th>Жанры</th>
                    <td><?= $FILM->getGenresVerbose() ?></td>
                </tr>
                <tr>
                    <th>Режиссёр</th>
                    <td><?= $FILM->director ?></td>
                </tr>
                <tr>
                    <th>Актёры</th>
                    <td><?= nl2br($FILM->actors) ?></td>
                </tr>
                </tbody>
            </table>
            <hr/>
            <p><?= nl2br($FILM->descr) ?></p>
        </div>
    </div>
<? } else { ?>
    <h2>Ошибка: Страница не существует</h2>
<? } ?>
