<?
/** @var Film $FILM */
if (!$ERROR) {
?>
<h2><?= $FILM->title ?>
    <div class="pull-right">
        <a href="/film/<?= $FILM->id ?>" class="btn btn-primary">View</a>
        <a href="/film/delete/<?= $FILM->id ?>" class="btn btn-danger">Delete</a>
    </div>
</h2>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group <?= $ERRORS['title'] ? 'has-error' : '' ?>">
                <label for="title" class="col-sm-3 control-label">Название</label>
                <div class="col-sm-9">
                    <input type="text" name="title" value="<?= $DATA['title'] ?>" class="form-control" id="title">
                    <?= $ERRORS['title'] ? '<span class="help-block">'.$ERRORS['title'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['original_title'] ? 'has-error' : '' ?>">
                <label for="original_title" class="col-sm-3 control-label">Оригинальное название</label>
                <div class="col-sm-9">
                    <input type="text" name="original_title" value="<?= $DATA['original_title'] ?>" class="form-control" id="original_title">
                    <?= $ERRORS['original_title'] ? '<span class="help-block">'.$ERRORS['original_title'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['country'] ? 'has-error' : '' ?>">
                <label for="country" class="col-sm-3 control-label">Страна</label>
                <div class="col-sm-9">
                    <input type="text" name="country" value="<?= $DATA['country'] ?>" class="form-control" id="country">
                    <?= $ERRORS['country'] ? '<span class="help-block">'.$ERRORS['country'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['director'] ? 'has-error' : '' ?>">
                <label for="director" class="col-sm-3 control-label">Режиссёр</label>
                <div class="col-sm-9">
                    <input type="text" name="director" value="<?= $DATA['director'] ?>" class="form-control" id="director">
                    <?= $ERRORS['director'] ? '<span class="help-block">'.$ERRORS['director'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['length'] ? 'has-error' : '' ?>">
                <label for="length" class="col-sm-3 control-label">Продолжительность</label>
                <div class="col-sm-2">
                    <input type="text" name="length" value="<?= $DATA['length'] ?>" class="form-control" id="length">
                    <?= $ERRORS['length'] ? '<span class="help-block">'.$ERRORS['length'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['filming_start'] ? 'has-error' : '' ?>">
                <label for="filming_start" class="col-sm-3 control-label">Годы начала</label>
                <div class="col-sm-2">
                    <input type="text" name="filming_start" value="<?= $DATA['filming_start'] ?>" class="form-control" id="filming_start">
                    <?= $ERRORS['filming_start'] ? '<span class="help-block">'.$ERRORS['filming_start'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['filming_end'] ? 'has-error' : '' ?>">
                <label for="filming_end" class="col-sm-3 control-label">и окончания съёмок</label>
                <div class="col-sm-2">
                    <input type="text" name="filming_end" value="<?= $DATA['filming_end'] ?>" class="form-control" id="filming_end">
                    <?= $ERRORS['filming_end'] ? '<span class="help-block">'.$ERRORS['filming_end'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['premiere'] ? 'has-error' : '' ?>">
                <label for="premiere" class="col-sm-3 control-label">Дата премьеры</label>
                <div class="col-sm-2">
                    <input type="date" name="premiere" value="<?= $DATA['premiere'] ?>" class="form-control" id="premiere">
                    <?= $ERRORS['premiere'] ? '<span class="help-block">'.$ERRORS['premiere'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['genres'] ? 'has-error' : '' ?>">
                <label for="genres" class="col-sm-3 control-label">Жанры</label>
                <div class="col-sm-9">
                    <input type="date" name="genres" value="<?= $DATA['genres'] ?>" class="form-control" id="genres">
                    <?= $ERRORS['genres'] ? '<span class="help-block">'.$ERRORS['genres'].'</span>' : '' ?>
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $("#genres").select2({tags: <?= json_encode($GENRES)?>, tokenSeparators: [","]});
                });
            </script>
            <div class="form-group <?= $ERRORS['actors'] ? 'has-error' : '' ?>">
                <label for="actors" class="col-sm-3 control-label">Список актёров</label>
                <div class="col-sm-9">
                    <textarea name="actors" class="form-control" id="actors"><?= $DATA['actors'] ?></textarea>
                    <?= $ERRORS['actors'] ? '<span class="help-block">'.$ERRORS['actors'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['short_descr'] ? 'has-error' : '' ?>">
                <label for="short_descr" class="col-sm-3 control-label">Анонс</label>
                <div class="col-sm-9">
                    <textarea name="short_descr" class="form-control" id="short_descr"><?= $DATA['short_descr'] ?></textarea>
                    <?= $ERRORS['short_descr'] ? '<span class="help-block">'.$ERRORS['short_descr'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['descr'] ? 'has-error' : '' ?>">
                <label for="descr" class="col-sm-3 control-label">Описание</label>
                <div class="col-sm-9">
                    <textarea name="descr" class="form-control" id="descr"><?= $DATA['descr'] ?></textarea>
                    <?= $ERRORS['descr'] ? '<span class="help-block">'.$ERRORS['descr'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group <?= $ERRORS['image'] ? 'has-error' : '' ?>">
                <label for="image" class="col-sm-3 control-label">Обложка</label>
                <div class="col-sm-9">
                    <? if ($FILM->image()) { ?><img src='<?= $FILM->getSmallImageUrl() ?>' width="100" height="145" class="img-thumbnail pull-left" style="margin-right: 20px;"><? } ?>
                    <input type="file" name="image"  id="image">
                    <?= $ERRORS['image'] ? '<span class="help-block">'.$ERRORS['image'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="/film/<?= $FILM->id ?>" class="btn btn-default">Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>
<? } else { ?>
    <h2>Ошибка: Страница не существует</h2>
<? } ?>
