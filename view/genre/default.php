<table class="table table-hover table-striped table-condensed">
    <thead>
    <tr>
        <th>#</th>
        <th>Жанр</th>
        <th>Количество фильмов</th>
    </tr>
    </thead>
    <tbody>
    <?
    if (count($DATA)) {
        foreach ($DATA as $i=>$ROW) {
            /** @var Genre $ROW */
        ?>
        <tr>
            <td><?= $i ?></td>
            <td><a href="/genre/edit/<?= $ROW->id ?>"><strong><?= $ROW->title ?></strong></a></td>
            <td><?= $ROW->getFilmsCount() ?></td>
            </tr>
        <?
        }
    } else { ?>
        <tr>
            <td colspan="7">Жанры отсутствуют</td>
        </tr>
    <? } ?>
    </tbody>
</table>
<? if (count($DATA) && $PAGES > 1) { ?>
    <ul class="pagination">
        <li <?= $PAGE == 1 ? 'class="disabled"' : '' ?>><a href="?page=1">&laquo;</a></li>
        <? for ($p = 1; $p <= $PAGES; $p++) { ?>
            <li <?= $PAGE == $p ? 'class="active"' : '' ?>><a href="?page=<?= $p ?>"><?= $p ?></a></li>
        <? } ?>
        <li <?= $PAGE == $PAGES ? 'class="disabled"' : '' ?>><a href="?page=<?= $PAGES ?>">&raquo;</a></li>
    </ul>
<? } ?>

