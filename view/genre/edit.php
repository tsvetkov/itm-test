<?
/** @var Genre $GENRE */
if (!$ERROR) {
?>
<h2><?= $GENRE->title ?>
    <div class="pull-right">
        <a href="/genre/delete/<?= $GENRE->id ?>" class="btn btn-danger">Delete</a>
    </div>
</h2>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group <?= $ERRORS['title'] ? 'has-error' : '' ?>">
                <label for="title" class="col-sm-3 control-label">Название</label>
                <div class="col-sm-9">
                    <input type="text" name="title" value="<?= $DATA['title'] ?>" class="form-control" id="title">
                    <?= $ERRORS['title'] ? '<span class="help-block">'.$ERRORS['title'].'</span>' : '' ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<? } else { ?>
    <h2>Ошибка: Страница не существует</h2>
<? } ?>
