<!DOCTYPE html>
<html>
<head>
    <title><?= $PROJECT ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery.js"></script>

    <link href="/js/select2/select2.css" rel="stylesheet"/>
    <link href="/js/select2/select2-bootstrap.css" rel="stylesheet"/>
    <script src="/js/select2/select2.js"></script>
    <style>
        body {
            padding-top: 70px;
        }
    </style>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><?= $PROJECT ?></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li <?= Core::uriParts(1) == '/' ? 'class="active"' : '' ?>><a href="/">Home</a></li>
                <li <?= Core::uriParts(2) == '/edit/add' ? 'class="active"' : '' ?>><a href="/edit/add">New Film</a></li>
                <li <?= Core::uriParts(1) == '/genre' ? 'class="active"' : '' ?>><a href="/genre">Genres</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container">
    <?= $CONTENT ?>
</div>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
