<table class="table table-hover table-striped table-condensed">
    <thead>
    <tr>
        <th>Обложка</th>
        <th>Фильм
<!--            <span class="glyphicon glyphicon-sort small text-muted"></span>-->
        </th>
        <th>Жанры</th>
        <th>Режиссёр</th>
        <th>Страна</th>
        <th>Продолжительность</th>
        <th>Дата премьеры</th>
    </tr>
    </thead>
    <tbody>
    <?
    if (count($DATA)) {
        foreach ($DATA as $ROW) {
            /** @var Film $ROW */
        ?>
        <tr>
            <td><? if ($ROW->image()) { ?><img src='<?= $ROW->getSmallImageUrl() ?>' width="100" height="145" class="img-thumbnail"><? } ?></td>
            <td>
                <p><a href="/film/<?= $ROW->id ?>"><strong><?= $ROW->title ?></strong></a>
                    <? if ($ROW->original_title) { ?><span class="small">(<?= $ROW->original_title ?>)</span><? } ?></p>
                <p><?= $ROW->short_descr ?></p>
            </td>
            <td><?= $ROW->getGenresVerbose() ?></td>
            <td><?= $ROW->director ?></td>
            <td><?= $ROW->country ?></td>
            <td><?= $ROW->getLengthVerbose() ?></td>
            <td><?= $ROW->getPremiereVerbose() ?></td>
            </tr>
        <?
        }
    } else { ?>
        <tr>
            <td colspan="7">Фильмы отсутствуют</td>
        </tr>
    <? } ?>
    </tbody>
</table>
<? if (count($DATA) && $PAGES > 1) { ?>
    <ul class="pagination">
        <li <?= $PAGE == 1 ? 'class="disabled"' : '' ?>><a href="?page=1">&laquo;</a></li>
        <? for ($p = 1; $p <= $PAGES; $p++) { ?>
            <li <?= $PAGE == $p ? 'class="active"' : '' ?>><a href="?page=<?= $p ?>"><?= $p ?></a></li>
        <? } ?>
        <li <?= $PAGE == $PAGES ? 'class="disabled"' : '' ?>><a href="?page=<?= $PAGES ?>">&raquo;</a></li>
    </ul>
<? } ?>
