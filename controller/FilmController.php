<?php

class FilmController extends Controller {

    function defaultAction() {
        $id = $this->request[0];

        $model = Film::bind($id);

        if ($model->exists()) {
            $this->view()->assign('film', $model);
        } else {
            $this->view()->assign('error', 1);
        }

        return $this->view()->parse()->get();
    }

    function deleteAction() {
        $id = $this->request[0];

        $model = Film::bind($id);

        if ($model->exists()) {
            $model->delete();
        }
        Core::redirect('/');

        return false;
    }

}