<?php

class EditController extends Controller {

    function defaultAction() {
        $id = $this->request[0];

        /** @var Film $model */
        $model = Film::bind($id);

        if ($model->exists()) {
            $genres = [];
            foreach (Genre::finder()->all() as $v) {
                $genres[] = $v->title;
            }

            if ($this->method == 'POST') {
                $data                  = array_merge($model->export(), $this->post);
                $model->title          = $this->post['title'];
                $model->original_title = $this->post['original_title'];
                $model->country        = $this->post['country'];
                $model->director       = $this->post['director'];
                $model->actors         = $this->post['actors'];
                $model->length         = $this->post['length'] ?: null;
                $model->premiere       = $this->post['premiere'] ?: null;
                $model->short_descr    = $this->post['short_descr'];
                $model->descr          = $this->post['descr'];
                $model->filming_start  = $this->post['filming_start'] ?: null;
                $model->filming_end    = $this->post['filming_end'] ?: null;
                $model->setGenres($this->post['genres']);
                $errors                = $model->validate();
                if ($this->files['image'] && $this->files['image']['tmp_name']) {
                    $file     = $this->files['image']['tmp_name'];
                    $img_data = getimagesize($file);
                    if (!$img_data) {
                        $errors['image'] = 'Для обложки необходимо изображение';
                    } elseif ($img_data[0] < 150 || $img_data[1] < 218) {
                        // вообще это конечно в тз не указано, но вообще-то нефиг
                        $errors['image'] = 'Для обложки необходимо изображение больше 150x218';
                    } else {
                        switch ($img_data['mime']) {
                            case 'image/jpeg':
                                $image = imagecreatefromjpeg($file);
                                break;
                            case 'image/png':
                                $image = imagecreatefrompng($file);
                                break;
                            case 'image/gif':
                                $image = imagecreatefromgif($file);
                                break;
                            default:
                                $image           = false;
                                $errors['image'] = 'Для обложки лучше использовать изображения форматов JPEG, PNG или GIF';
                                break;
                        }

                        if (!$errors['image']) {
                            $filename = Core::config('upload_dir') . '/' . $model->id;
                            $w        = $img_data[0];
                            $h        = $img_data[1];
                            if ($img_data['mime'] != 'image/jpeg') {
                                $image_p = imagecreatetruecolor($w, $h);
                                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $w, $h, $w, $h);
                                imagejpeg($image_p, $filename . '.jpg');
                                imagedestroy($image_p);
                            } else {
                                move_uploaded_file($file, $filename . '.jpg');
                            }

                            // todo уничтожить копипасту
                            $image_p = imagecreatetruecolor(150, 218);
                            $src_x   = $src_y = 0;
                            $src_w   = $w;
                            $src_h   = $h;
                            $cmp_x   = $w / 150;
                            $cmp_y   = $h / 218;
                            if ($cmp_x > $cmp_y) {
                                $src_w = round($w / $cmp_x * $cmp_y);
                                $src_x = round(($w - ($w / $cmp_x * $cmp_y)) / 2);
                            } else {
                                if ($cmp_y > $cmp_x) {
                                    $src_h = round($h / $cmp_y * $cmp_x);
                                    $src_y = round(($h - ($h / $cmp_y * $cmp_x)) / 2);
                                }
                            }
                            imagecopyresampled($image_p, $image, 0, 0, $src_x, $src_y, 150, 218, $src_w, $src_h);
                            imagejpeg($image_p, $filename . '_150x218.jpg');
                            imagedestroy($image_p);

                            $image_p = imagecreatetruecolor(100, 145);
                            $src_x   = $src_y = 0;
                            $src_w   = $w;
                            $src_h   = $h;
                            $cmp_x   = $w / 100;
                            $cmp_y   = $h / 145;
                            if ($cmp_x > $cmp_y) {
                                $src_w = round($w / $cmp_x * $cmp_y);
                                $src_x = round(($w - ($w / $cmp_x * $cmp_y)) / 2);
                            } else {
                                if ($cmp_y > $cmp_x) {
                                    $src_h = round($h / $cmp_y * $cmp_x);
                                    $src_y = round(($h - ($h / $cmp_y * $cmp_x)) / 2);
                                }
                            }
                            imagecopyresampled($image_p, $image, 0, 0, $src_x, $src_y, 100, 145, $src_w, $src_h);
                            imagejpeg($image_p, $filename . '_100x145.jpg');
                            imagedestroy($image_p);
                        }
                    }
                }
                if (empty($errors)) {
                    $model->save();
                    Core::redirect('/film/' . $model->id);
                } else {
                    $model->revert();
                }
            } else {
                $data   = $model->export();
                $data['genres'] = $model->getGenresVerbose();
                $errors = [];
            }

            $this->view()->assign('film', $model);
            $this->view()->assign('genres', $genres);
            $this->view()->assign('data', $data);
            $this->view()->assign('errors', $errors);
        } else {
            $this->view()->assign('error', 1);
        }

        return $this->view()->parse()->get();
    }

    function addAction() {
        $genres = [];
        foreach (Genre::finder()->all() as $v) {
            $genres[] = $v->title;
        }

        if ($this->method == 'POST') {
            $data                  = $this->post;
            /** @var Film $model */
            $model                 = Film::bind(0);
            $model->title          = $this->post['title'];
            $model->original_title = $this->post['original_title'];
            $model->country        = $this->post['country'];
            $model->director       = $this->post['director'];
            $model->actors         = $this->post['actors'];
            $model->length         = $this->post['length'] ?: null;
            $model->premiere       = $this->post['premiere'] ?: null;
            $model->short_descr    = $this->post['short_descr'];
            $model->descr          = $this->post['descr'];
            $model->filming_start  = $this->post['filming_start'] ?: null;
            $model->filming_end    = $this->post['filming_end'] ?: null;
            $model->setGenres($this->post['genres']);
            $errors                = $model->validate();
            if ($this->files['image'] && $this->files['image']['tmp_name']) {
                $file     = $this->files['image']['tmp_name'];
                $img_data = getimagesize($file);
                if (!$img_data) {
                    $errors['image'] = 'Для обложки необходимо изображение';
                } elseif ($img_data[0] < 150 || $img_data[1] < 218) {
                    // вообще это конечно в тз не указано, но вообще-то нефиг
                    $errors['image'] = 'Для обложки необходимо изображение больше 150x218';
                } else {
                    switch ($img_data['mime']) {
                        case 'image/jpeg':
                            $image = imagecreatefromjpeg($file);
                            break;
                        case 'image/png':
                            $image = imagecreatefrompng($file);
                            break;
                        case 'image/gif':
                            $image = imagecreatefromgif($file);
                            break;
                        default:
                            $image           = false;
                            $errors['image'] = 'Для обложки лучше использовать изображения форматов JPEG, PNG или GIF';
                            break;
                    }

                    if (!$errors['image']) {
                        $filename = Core::config('upload_dir') . '/' . $model->id;
                        $w        = $img_data[0];
                        $h        = $img_data[1];
                        if ($img_data['mime'] != 'image/jpeg') {
                            $image_p = imagecreatetruecolor($w, $h);
                            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $w, $h, $w, $h);
                            imagejpeg($image_p, $filename . '.jpg');
                            imagedestroy($image_p);
                        } else {
                            move_uploaded_file($file, $filename . '.jpg');
                        }

                        // todo уничтожить копипасту
                        $image_p = imagecreatetruecolor(150, 218);
                        $src_x   = $src_y = 0;
                        $src_w   = $w;
                        $src_h   = $h;
                        $cmp_x   = $w / 150;
                        $cmp_y   = $h / 218;
                        if ($cmp_x > $cmp_y) {
                            $src_w = round($w / $cmp_x * $cmp_y);
                            $src_x = round(($w - ($w / $cmp_x * $cmp_y)) / 2);
                        } else {
                            if ($cmp_y > $cmp_x) {
                                $src_h = round($h / $cmp_y * $cmp_x);
                                $src_y = round(($h - ($h / $cmp_y * $cmp_x)) / 2);
                            }
                        }
                        imagecopyresampled($image_p, $image, 0, 0, $src_x, $src_y, 150, 218, $src_w, $src_h);
                        imagejpeg($image_p, $filename . '_150x218.jpg');
                        imagedestroy($image_p);

                        $image_p = imagecreatetruecolor(100, 145);
                        $src_x   = $src_y = 0;
                        $src_w   = $w;
                        $src_h   = $h;
                        $cmp_x   = $w / 100;
                        $cmp_y   = $h / 145;
                        if ($cmp_x > $cmp_y) {
                            $src_w = round($w / $cmp_x * $cmp_y);
                            $src_x = round(($w - ($w / $cmp_x * $cmp_y)) / 2);
                        } else {
                            if ($cmp_y > $cmp_x) {
                                $src_h = round($h / $cmp_y * $cmp_x);
                                $src_y = round(($h - ($h / $cmp_y * $cmp_x)) / 2);
                            }
                        }
                        imagecopyresampled($image_p, $image, 0, 0, $src_x, $src_y, 100, 145, $src_w, $src_h);
                        imagejpeg($image_p, $filename . '_100x145.jpg');
                        imagedestroy($image_p);
                    }
                }
            }
            if (empty($errors)) {
                $model->save();
                Core::redirect('/film/' . $model->id);
            } else {
                $model->revert();
            }
        } else {
            $data   = [];
            $errors = [];
        }

        $this->view()->assign('data', $data);
        $this->view()->assign('genres', $genres);
        $this->view()->assign('errors', $errors);


        return $this->view()->parse()->get();
    }
}