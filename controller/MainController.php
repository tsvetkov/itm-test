<?php

class MainController extends Controller {

    function defaultAction() {
        $ppage = 20;
        $page  = array_key_exists('page', $this->get) ? $this->get['page'] : 1;
        if ((int)$page <= 0) {
            $page = 1;
        }

        $data = Film::finder()->order('title')->start(($page - 1) * $ppage)->limit($ppage)->all();

        if ($data) {
            $pages = (int)($data->total() / $ppage);
            if ($data->total() % $ppage > 0) {
                $pages++;
            }
            if ($pages > 0 && $page > $pages) {
                $page = $pages;
                $data = Film::finder()->order('title')->start(($page - 1) * $ppage)->limit($ppage)->all();
            }
            $this->view()->assign('data', $data);
            $this->view()->assign('page', $page);
            $this->view()->assign('pages', $pages);
        } else {
            $this->view()->assign('data', []);
        }

        return $this->view()->parse()->get();
    }

}