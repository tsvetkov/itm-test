<?php

class GenreController extends Controller {

    // todo тут нет добавления и просмотра потому что так задумано

    function defaultAction() {
        $ppage = 20;
        $page  = array_key_exists('page', $this->get) ? $this->get['page'] : 1;
        if ((int)$page <= 0) {
            $page = 1;
        }

        $data = Genre::finder()->order('title')->start(($page - 1) * $ppage)->limit($ppage)->all();

        if ($data) {
            $pages = (int)($data->total() / $ppage);
            if ($data->total() % $ppage > 0) {
                $pages++;
            }
            if ($pages > 0 && $page > $pages) {
                $page = $pages;
                $data = Genre::finder()->order('title')->start(($page - 1) * $ppage)->limit($ppage)->all();
            }
            $this->view()->assign('data', $data);
            $this->view()->assign('page', $page);
            $this->view()->assign('pages', $pages);
        } else {
            $this->view()->assign('data', []);
        }

        return $this->view()->parse()->get();
    }

    function editAction() {
        $id = $this->request[0];

        /** @var Film $model */
        $model = Genre::bind($id);

        if ($model->exists()) {
            if ($this->method == 'POST') {
                $data                  = array_merge($model->export(), $this->post);
                $model->title          = $this->post['title'];
                $errors                = $model->validate();
                if (empty($errors)) {
                    $model->save();
                    Core::redirect('/genre/');
                } else {
                    $model->revert();
                }
            } else {
                $data   = $model->export();
                $errors = [];
            }

            $this->view()->assign('genre', $model);
            $this->view()->assign('data', $data);
            $this->view()->assign('errors', $errors);
        } else {
            $this->view()->assign('error', 1);
        }

        return $this->view()->parse()->get();
    }

    function deleteAction() {
        $id = $this->request[0];

        $model = Genre::bind($id);

        if ($model->exists()) {
            $model->delete();
        }

        Core::redirect('/genre/');

        return false;
    }
}